#include <Application.h>

#include "PanelWindow.h"
#include "SettingsFile.h"
#include "LnList.h"

#include <Catalog.h>
#include <Locale.h>

#undef B_TRANSLATION_CONTEXT
#define B_TRANSLATION_CONTEXT "LnLauncher"

class MyApp : public BApplication

{
	protected:
		List<PanelWindow>	m_panels;
		bool				fAutoStart;
		
	public:
		MyApp()
		:	BApplication("application/x-vnd.LnLauncher")
		{
			fAutoStart = true;
		};
		
		virtual ~MyApp()
		{
		};
		
		virtual thread_id Run()
		{
			BMessage msg;
			SettingsFile settings;
			
			settings.Load();
			
			settings.FindBool("autostart", fAutoStart);
			for ( int32 c=0; settings.FindMessage("panel",c,&msg) == B_OK; c++ )
			{
				PanelWindow * panel = dynamic_cast<PanelWindow*>( PanelWindow::Instantiate(&msg) );
				m_panels += panel;
				
				panel->Show();
			}
			
			if ( m_panels.NumItems() == 0 )
			{
				PanelWindow * panel = new PanelWindow();
				m_panels += panel;
				panel->Show();
			}
			
			return BApplication::Run();
		};
		
		void SaveSettings()
		{
			SettingsFile settings;
			BMessage panel;
			
			settings.MakeEmpty();

			settings.AddBool("autostart", fAutoStart);
					
			for ( int c=0; m_panels[c]; c++ )
			{
				panel.MakeEmpty();
				
				m_panels[c]->Lock();
				m_panels[c]->Archive( &panel, true );
				m_panels[c]->Unlock();
				
				settings.AddMessage("panel",&panel );
			}
			
			settings.Save();
		};
		
		virtual void MessageReceived( BMessage * msg )
		{
			switch ( msg->what )
			{
				case 'Npan':
				{ // new panel
					PanelWindow * panel = new PanelWindow();
					m_panels += panel;
					panel->Show();
				}	break;
				case 'Rpan':
				{ // remove panel
					if ( m_panels.NumItems() < 2 )
						break;
					
					PanelWindow * panel = NULL;
					
					msg->FindPointer("panel",(void**)&panel);
					
					m_panels -= panel;
					
					panel->Lock();
					panel->Quit();
					SaveSettings();
				}	break;
				case 'Uset':
				{ // update settings
					SaveSettings();
				}	break;
				case 'Auto':
				{
					msg->FindBool("autostart", &fAutoStart);
					SaveSettings();
				}	break;
				default:
					return BApplication::MessageReceived( msg );
					
			}
		};
};

int main( void )
{
	MyApp app;
	
	app.Run();
	
	return 0;
}
