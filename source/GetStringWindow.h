#ifndef GET_STRING_WINDOW_H
#define GET_STRING_WINDOW_H

#include <Window.h>
#include <Messenger.h>

#define GET_STRING_WINDOW_RESULT	'gswR'

typedef void (* got_string_func)(void * arg, const char * str);

class GetStringWindow : public BWindow
{
	public:
		GetStringWindow( BMessenger target, const char * label, const char * text );
		GetStringWindow( got_string_func, void * arg, const char * label, const char * text );
		virtual ~GetStringWindow();
		
		virtual void MessageReceived( BMessage * );
		
	private:
		void CommonInit(const char*, const char*);
		
		BMessenger			m_target;
		got_string_func		m_func;
		void 				* m_arg;
};

#endif
