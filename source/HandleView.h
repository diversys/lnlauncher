#ifndef HANDLE_VIEW_H
#define HANDLE_VIEW_H

#include <View.h>

class BMenu;
class BMenuItem;
class BPopUpMenu;

class HandleView : public BView
{
	public:
		HandleView();
		HandleView( BMessage* );
		virtual ~HandleView();
		
		static BArchivable * Instantiate( BMessage * );
		
		virtual status_t Archive( BMessage *, bool ) const;
		
		virtual void MouseDown( BPoint );
		virtual void MouseMoved( BPoint, uint32, const BMessage * );
		virtual void MouseUp( BPoint );
		
		virtual void Draw( BRect );
		virtual void MessageReceived( BMessage * );
		
	protected:
		bool	m_mouse_down;
		bool	m_moving_panel;
		int32	m_buttons;

		BPopUpMenu* fMenu;
		BMenu* fIconSizeMenu;
		BMenu* fThicknessMenu;
		BMenu* fScrollSpeedMenu;
		BMenu* fExpandMenu;
		BMenu* fFloatMenu;
		BMenu* fReplicantsMenu;
		BMenuItem* fAutoStartMenu;
		
		void ShowPopup( BPoint );
};

#endif
