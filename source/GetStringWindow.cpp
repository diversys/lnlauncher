#include "GetStringWindow.h"

#include <TextControl.h>
#include <Button.h>
#include <Screen.h>
#include <stdio.h>
#include <Messenger.h>
#include <View.h>
#include <Catalog.h>
#include <Locale.h>

#undef B_TRANSLATION_CONTEXT
#define B_TRANSLATION_CONTEXT ("ArgumentWindow")

static const char* kOk = B_TRANSLATE_MARK("Ok");
static const char* kCancel = B_TRANSLATE_MARK("Cancel");
static const char* kGetString = B_TRANSLATE_MARK("Get String");
static const char* kGetStringTop = B_TRANSLATE_MARK("Get String");


GetStringWindow::GetStringWindow( BMessenger target, const char * label, const char * text)
	:	BWindow(BRect(0, 0, 300, 65), B_TRANSLATE(kGetStringTop),B_TITLED_WINDOW,
					B_ASYNCHRONOUS_CONTROLS | B_NOT_RESIZABLE | B_NOT_ZOOMABLE),
						m_target( target ),
						m_func( NULL ),
						m_arg( NULL )
	{
		if ( !m_target.IsValid() )
			{
				printf("GetStringWindow: Bad messenger\n");
			}
	}


GetStringWindow::GetStringWindow( got_string_func func, void * arg, const char * label, const char * text )
	:	BWindow(BRect(0, 0, 300, 65), B_TRANSLATE(kGetStringTop),B_TITLED_WINDOW,
					B_ASYNCHRONOUS_CONTROLS | B_NOT_RESIZABLE | B_NOT_ZOOMABLE),
					m_func( func ),
					m_arg( arg )
	{	
		CommonInit(label, text);
	}


void
GetStringWindow::CommonInit( const char * label, const char * text )
	{
		float y_middle = Bounds().Height() / 2;
		float x_middle = Bounds().Width() / 2;
		
		BView *view = new BView(Frame(), "background", B_FOLLOW_ALL, B_WILL_DRAW);
		AddChild(view);
		view->SetViewColor(ui_color(B_PANEL_BACKGROUND_COLOR));

		BTextControl * control = new BTextControl(
			BRect(15, 5, 280, 10), "control", label, text, new BMessage); 
		view->AddChild(control);

		BFont font;
		control->GetFont( &font );
		control->SetDivider( font.StringWidth(label)*1.1 );
		
		BRect button_rect(15, 0, x_middle - 20, y_middle - 20);
		y_middle = control->Frame().bottom + 1;	
	
		BButton * cancel = new BButton(
			button_rect, "", B_TRANSLATE(kCancel), 
				new BMessage('Canc'));
		view->AddChild(cancel);
		cancel->MoveBy(0, y_middle + 5);
	
		BButton * ok = new BButton(
			button_rect, "", B_TRANSLATE(kOk), 
				new BMessage('Ok  '));
		view->AddChild(ok);
		ok->MoveBy( x_middle, y_middle + 5 );	
		
		CenterOnScreen();
	}


GetStringWindow::~GetStringWindow()
{
}


void
GetStringWindow::MessageReceived( BMessage * msg )
{
	switch ( msg->what )
	{
		case 'Ok  ':
		{ // Ok
			BTextControl * control = (BTextControl*)FindView("control");
			
			if ( m_target.IsValid() )
			{
				BMessage trg_msg( GET_STRING_WINDOW_RESULT ), reply;
				trg_msg.AddString("string", control->Text() );
			
				trg_msg.PrintToStream();
			
				if ( m_target.SendMessage( &trg_msg, &reply ) != B_OK )
					printf("  send message fail\n");
			}
			if ( m_func )
			{
				m_func(m_arg, control->Text());
			}
		}; // fall through
		case 'Canc':
		{ // Cancel, close window
			PostMessage( B_QUIT_REQUESTED );
		}	break;
		default:
			BWindow::MessageReceived( msg );
	}
}
